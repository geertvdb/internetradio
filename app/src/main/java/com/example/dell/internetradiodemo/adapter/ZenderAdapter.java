package com.example.dell.internetradiodemo.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dell.internetradiodemo.R;
import com.example.dell.internetradiodemo.model.Zender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 26/04/2017.
 */

public class ZenderAdapter extends ArrayAdapter {

    private List list = new ArrayList<>();
    private int recource;

    public ZenderAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        this.recource = resource;
    }

    @Override
    public void add(@Nullable Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row;
        row = convertView;
        ViewHolder holder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(recource, parent, false);
            holder = new ViewHolder();
            holder.Zender_image = (ImageView) row.findViewById(R.id.ivZender_image);
            holder.Zender_naam = (TextView) row.findViewById(R.id.tvZender);
            row.setTag(holder);
        }else{
            holder = (ViewHolder) row.getTag();
        }

        Zender zender;
        zender = (Zender) this.getItem(position);
        holder.Zender_image.setImageResource(zender.getZender_image());
        holder.Zender_naam.setText(zender.getZender_naam());

        return row;
    }

    static class ViewHolder{
        ImageView Zender_image;
        TextView Zender_naam;
    }
}
