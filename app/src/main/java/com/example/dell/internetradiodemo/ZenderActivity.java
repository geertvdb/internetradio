package com.example.dell.internetradiodemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.io.IOException;

public class ZenderActivity extends AppCompatActivity {

    private ImageView imageView;
    private ProgressDialog progressDialog = null;
    private MediaPlayer mediaPlayer;
    private Button btnStart;
    private MusicStreamTask musicStreamTask;
    private boolean prepared = false;
    private boolean started = false;
    private Intent serviceIntent;
    private SeekBar volumeSeekbar = null;
    private AudioManager audioManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zender);

        imageView = (ImageView) findViewById(R.id.ivZenderLogo);
        btnStart = (Button) findViewById(R.id.btnPlay);
        volumeSeekbar = (SeekBar) findViewById(R.id.sbVolume);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        volumeSeekbar.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeSeekbar.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));

        try {
            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        int zenderLogo = intent.getIntExtra("ZenderImage", 0);
        final String zenderURL = intent.getStringExtra("Zender");
        imageView.setImageResource(zenderLogo);


        ///////////////////// SERVICE TEST /////////////////////////
        //Intent serviceIntent = new Intent(BackgroundAudioService.class.getName());
        /*serviceIntent = new Intent(this,BackgroundAudioService.class);
        serviceIntent.putExtra("zender", zenderURL);
        startService(serviceIntent);
        started = false; */


        musicStreamTask = new MusicStreamTask();
        musicStreamTask.execute(zenderURL);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (started) {
                    started = false;
                    mediaPlayer.pause();
                    //startService(serviceIntent);
                    btnStart.setText("Play");
                } else {
                    started = true;
                    mediaPlayer.start();
                    //stopService(serviceIntent);
                    btnStart.setText("Pause");
                }

            }
        });
    }

    private class MusicStreamTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ZenderActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading....");
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                mediaPlayer.setDataSource(params[0]);
                mediaPlayer.prepare();
                prepared = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mediaPlayer.start();
            btnStart.setEnabled(true);
            btnStart.setText("Pauze");
            mediaPlayer.start();
            started = true;
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (started) {
            // mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (started) {
            //mediaPlayer.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (prepared) {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }
        //stopService(serviceIntent);
        //wakeLock.release();
    }

}
