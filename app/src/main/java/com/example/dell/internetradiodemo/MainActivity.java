package com.example.dell.internetradiodemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dell.internetradiodemo.adapter.ZenderAdapter;
import com.example.dell.internetradiodemo.model.Zender;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lstList;
    private TextView tvText;

    private int[] zenders = {R.drawable.radioeen, R.drawable.brussel, R.drawable.mnm, R.drawable.qmusic,
            R.drawable.contact, R.drawable.topradio, R.drawable.sporza, R.drawable.rockfm};
    private String[] zender_name;
    private ZenderAdapter adapter;
    private final String radioEen = "http://icecast.vrtcdn.be/radio1-high.mp3 ";
    private final String studioBrussel = "http://icecast.vrtcdn.be/stubru-mid.mp3";
    private final String mnm = "http://icecast.vrtcdn.be/mnm-high.mp3";
    private final String qmusic = "http://icecast-qmusic.cdp.triple-it.nl/Qmusic_be_live_128.mp3";
    private final String contact = "http://audiostream.rtl.be/contactfr192";
    private final String topradio = "http://loadbalancing.topradio.be/topradio.mp3";
    private final String sporza = "http://icecast.vrtcdn.be/sporza-high.mp3";
    private final String rockfm = "http://streams.movemedia.eu:8440/;stream";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lstList = (ListView) findViewById(R.id.lstList_stations);
        tvText = (TextView) findViewById(R.id.tvText);
        zender_name = getResources().getStringArray(R.array.station_list);

        adapter = new ZenderAdapter(getApplicationContext(), R.layout.row_item);

        int i = 0;
        for (String name : zender_name) {
            Zender zender = new Zender(zenders[i], name);
            adapter.add(zender);
            i++;
        }

        lstList.setAdapter(adapter);

        lstList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvZender = (TextView) view.findViewById(R.id.tvZender);
                String zender = tvZender.getText().toString();

                if (zender.equals("Radio 1")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.radioeen);
                    intent.putExtra("Zender", radioEen);
                    startActivity(intent);

                } else if (zender.equals("Studio Brussel")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.brussel);
                    intent.putExtra("Zender", studioBrussel);
                    startActivity(intent);

                } else if (zender.equals("MNM")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.mnm);
                    intent.putExtra("Zender", mnm);
                    startActivity(intent);

                } else if (zender.equals("Q-Music")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.qmusic);
                    intent.putExtra("Zender", qmusic);
                    startActivity(intent);

                } else if (zender.equals("Contact")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.contact);
                    intent.putExtra("Zender", contact);
                    startActivity(intent);

                } else if (zender.equals("TOP-Radio")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.topradio);
                    intent.putExtra("Zender", topradio);
                    startActivity(intent);

                } else if (zender.equals("Sporza Radio")) {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.sporza);
                    intent.putExtra("Zender", sporza);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(MainActivity.this, ZenderActivity.class);
                    intent.putExtra("ZenderImage", R.drawable.rockfm);
                    intent.putExtra("Zender", rockfm);
                    startActivity(intent);
                }
            }
        });

    }
}
