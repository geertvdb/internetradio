package com.example.dell.internetradiodemo.model;

/**
 * Created by Dell on 26/04/2017.
 */

public class Zender {

    private int zender_image;
    private String zender_naam;

    public Zender(int zender_image, String zender_naam) {
        this.setZender_image(zender_image);
        this.setZender_naam(zender_naam);
    }

    public int getZender_image() {
        return zender_image;
    }

    public void setZender_image(int zender_image) {
        this.zender_image = zender_image;
    }

    public String getZender_naam() {
        return zender_naam;
    }

    public void setZender_naam(String zender_naam) {
        this.zender_naam = zender_naam;
    }
}
